package com.example.daparcoc.laboratorio101_aplicacion1;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button btnActivar;
    private Button btnDesactivar;
    private TextView lblLatitud;
    private TextView lblLongitud;
    private TextView lblPrecision;
    private TextView lblEstado;
    private LocationManager locManager;
    private LocationListener locListener;

    private TextView mTextView;
    private static final long MIN_DISTANCE = 5;
    private static final long MIN_TIME = 10*1000; // 10 segundos
    private LocationManager mLocationManager;
    private String mProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnActivar = (Button) findViewById(R.id.BtnActivar);
        btnDesactivar = (Button) findViewById(R.id.BtnDesactivar);
        lblLatitud = (TextView) findViewById(R.id.LblLatitud);
        lblLongitud = (TextView) findViewById(R.id.LblLongitud);
        lblPrecision = (TextView) findViewById(R.id.LblPrecision);
        lblEstado = (TextView) findViewById(R.id.LblEstado);

        btnActivar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comenzarLocalizacion();
            }
        });
        btnDesactivar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    locManager.removeUpdates(locListener);
                } catch (SecurityException se) {
                }
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void comenzarLocalizacion() {
        try {
            locManager = (LocationManager)
                    getSystemService(Context.LOCATION_SERVICE);
            Location loc = locManager.getLastKnownLocation(
                    LocationManager.GPS_PROVIDER);
            mostrarPosicion(loc);
            locListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    mostrarPosicion(location);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                    Log.i("", "Provider Status: " + status);
                    lblEstado.setText("Provider Status: " + status);
                }

                @Override
                public void onProviderEnabled(String provider) {
                    lblEstado.setText("Provide ON");
                }

                @Override
                public void onProviderDisabled(String provider) {
                    lblEstado.setText("Provider OFF");
                }
            };
            locManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 30000, 0, locListener);
        } catch (SecurityException se) {
        }
    }

    private void mostrarPosicion(Location loc) {
        if (loc != null) {
            lblLatitud.setText("Latitud: " +
                String.valueOf(loc.getLatitude()));
            lblLongitud.setText("Longitud: " +
                String.valueOf(loc.getLongitude()));
            lblPrecision.setText("Precision: " +
                String.valueOf(loc.getAccuracy()));
            Log.i("latlong", String.valueOf(loc.getLatitude()) + "-" +
                String.valueOf(loc.getLongitude()));
        } else {
            lblLatitud.setText("Latitud: (sin_datos)");
            lblLongitud.setText("Longitud: (sin_datos)");
            lblPrecision.setText("Precision: (sin_datos)");
        }
    }
}
